
###      Table of content
#### 1 Background 
#### 2 Install 
#### 3 Run 
#### 4 Usage 
#### 5 Diagram 
#### 6 Test
#### 7 Maintainers 
#----------------------------------------------
###     1 Background
Java-based console game with 4 characters.
Each character can be modified and become stronger.

###    2 Install
To set up the development environment you need Intellij
with at least JDK 17

###    3 Run
To run the game you need to use System.out.println() which has character in 
parentheses which causes all functions associated with character can be called.
Example:    
###            Mage mage = new Mage("Mage",1,0,new PrimaryAttributes(1,1,8),
###            new TotalAttributes(0,0,0),
###            new Armor("Armor",2,new HashMap<>(),new PrimaryAttributes(0,0,0)),
###            new Weapons(2,new HashMap<>(),0)
###             );
###            mage.checkIfHeroHaveWeaponAttributes();
###            mage.levelUp();
###            mage.addArmorAttributes();
###            mage.getAttributesTotalSum();
###            mage.addWeaponAttributes();
###            mage.countOutCharacterDPS();
##    -------> System.out.println(mage);      <-------


###    4 Usage
The game has an abstract Character class that contains abstract methods and each Hero
has its own class that extends all functionality from Character.
I created  two exception classes that are thrown when needed.
In the game there is also Item abstract class which is inherited by class Armor and Weapons.
You will get a better explanation in the Diagram below.

###    5 Diagram
![](Diagram.jpg)

###    6 Test
Full test coverage of the functionality using JUnit 5.

###    7 Maintainers
@lynxxxxx
Rinat Iunusov

