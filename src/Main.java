import attributes.PrimaryAttributes;
import attributes.TotalAttributes;
import character.Character;
import character.Mage;
import character.Ranger;
import character.Rogue;
import character.Warrior;
import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import items.Armor;
import items.Weapons;

import java.util.HashMap;

public class Main {
    public static void main(String[] args) throws InvalidArmorException, InvalidWeaponException {
        Mage mage = new Mage("Mage",1,0,new PrimaryAttributes(1,1,8),
                new TotalAttributes(0,0,0),
                new Armor("Armor",2,new HashMap<>(),new PrimaryAttributes(0,0,0)),
                new Weapons(2,new HashMap<>(),0)
                );
        mage.checkIfHeroHaveWeaponAttributes();
        mage.levelUp();
        mage.addArmorAttributes();
        mage.getAttributesTotalSum();
        mage.addWeaponAttributes();
        mage.countOutCharacterDPS();
        mage.levelUp();
        mage.levelUp();








        Ranger ranger = new Ranger("Ranger",1,0,new PrimaryAttributes(1,7,1),
                new TotalAttributes(0,0,0),
                new Armor("Armor",2,new HashMap<>(),new PrimaryAttributes(0,0,0)),
                new Weapons(2,new HashMap<>(),0)
        );
        ranger.checkIfHeroHaveWeaponAttributes();
        ranger.levelUp();
        ranger.addArmorAttributes();
        ranger.getAttributesTotalSum();
        ranger.addWeaponAttributes();
        ranger.countOutCharacterDPS();
        ranger.levelUp();



        Rogue rogue = new Rogue("Rogue",1,0,new PrimaryAttributes(2,6,1),
                new TotalAttributes(0,0,0),
                new Armor("Armor",2,new HashMap<>(),new PrimaryAttributes(0,0,0)),
                new Weapons(2,new HashMap<>(),0)
        );
        rogue.checkIfHeroHaveWeaponAttributes();
        rogue.levelUp();
        rogue.addArmorAttributes();
        rogue.getAttributesTotalSum();
        rogue.addWeaponAttributes();
        rogue.countOutCharacterDPS();
        rogue.levelUp();



        Warrior warrior = new Warrior("Warrior",1,0,new PrimaryAttributes(5,2,1),
                new TotalAttributes(0,0,0),
                new Armor("Armor",2,new HashMap<>(),new PrimaryAttributes(0,0,0)),
                new Weapons(2,new HashMap<>(),0)
        );
        warrior.checkIfHeroHaveWeaponAttributes();
        warrior.levelUp();
        warrior.addWeaponAttributes();
        warrior.addArmorAttributes();
        warrior.getAttributesTotalSum();

        warrior.countOutCharacterDPS();
        warrior.levelUp();



        System.out.println(mage);
        mage.characterDisplay();

        System.out.println(ranger);
        ranger.characterDisplay();

        System.out.println(rogue);
        rogue.characterDisplay();

        System.out.println(warrior);
        warrior.characterDisplay();
    }
}