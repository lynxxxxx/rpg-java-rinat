package attributes;

import java.util.Objects;

public class PrimaryAttributes {

    private double strength;
    private double dexterity;

    private double intelligence;

    public PrimaryAttributes(double strength, double dexterity, double intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public double getDexterity() {
        return dexterity;
    }

    public void setDexterity(double dexterity) {
        this.dexterity = dexterity;
    }

    public double getStrength() {
        return strength;
    }

    public void setStrength(double strength) {
        this.strength = strength;
    }

    public double getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(double intelligence) {
        this.intelligence = intelligence;
    }


    @Override
    public boolean equals(Object o) {
        PrimaryAttributes comp = (PrimaryAttributes) o;
        if(this.intelligence == comp.intelligence
                && this.dexterity == comp.dexterity
                && this.strength == comp.strength) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "PrimaryAttributes{" +
                "strength=" + strength +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence +
                '}';
    }
}
