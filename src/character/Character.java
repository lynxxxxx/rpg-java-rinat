package character;

import attributes.PrimaryAttributes;
import attributes.TotalAttributes;
import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import items.Armor;
import items.Weapons;


public abstract class Character {

    protected String name;
    protected double level;

    protected double characterDPS;
    protected PrimaryAttributes primaryAttributes;
    protected TotalAttributes totalAttributes;

    protected Armor armor;
    protected Weapons weapons;


    public Character() {
    }

    public Character(String name, double level, double characterDPS, PrimaryAttributes primaryAttributes, TotalAttributes totalAttributes, Armor armor, Weapons weapons) {
        this.name = name;
        this.level = level;
        this.characterDPS = characterDPS;
        this.primaryAttributes = primaryAttributes;
        this.totalAttributes = totalAttributes;
        this.armor = armor;
        this.weapons = weapons;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLevel() {
        return level;
    }

    public void setLevel(double level) {
        this.level = level;
    }

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public void setPrimaryAttributes(PrimaryAttributes primaryAttributes) {
        this.primaryAttributes = primaryAttributes;
    }

    public TotalAttributes getTotalAttributes() {
        return totalAttributes;
    }

    public void setTotalAttributes(TotalAttributes totalAttributes) {
        this.totalAttributes = totalAttributes;
    }

    public Armor getArmor() {
        return armor;
    }

    public void setArmor(Armor armor) {
        this.armor = armor;
    }

    public Weapons getWeapons() {
        return weapons;
    }

    public void setWeapons(Weapons weapons) {
        this.weapons = weapons;
    }

    public double getCharacterDPS() {
        return characterDPS;
    }

    public void setCharacterDPS(double characterDPS) {
        this.characterDPS = characterDPS;
    }


    /**
     * all characters can use these abstract methods
     */
    public abstract void levelUp();

    public abstract void addArmorAttributes() throws InvalidArmorException;

    public abstract void getAttributesTotalSum();

    public abstract void addWeaponAttributes() throws InvalidWeaponException;

    public abstract void checkIfHeroHaveWeaponAttributes();

    public abstract void countOutCharacterDPS();


    public void characterDisplay() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\r\n Name: ").append(getName());
        stringBuilder.append("\r\n Level: ").append(getLevel());
        stringBuilder.append("\r\n Strength: ").append(getTotalAttributes().getStrength());
        stringBuilder.append("\r\n Dexterity: ").append(getTotalAttributes().getDexterity());
        stringBuilder.append("\r\n Intelligence: ").append(getTotalAttributes().getIntelligence());
        stringBuilder.append("\r\n CharacterDPS: ").append(getCharacterDPS()).append("\n");

        System.out.println(stringBuilder);
    }

    @Override
    public String toString() {
        return "Character{" +
                "name='" + name + '\'' +
                ", level=" + level +
                ", characterDPS=" + characterDPS +
                ", primaryAttributes=" + primaryAttributes +
                ", totalAttributes=" + totalAttributes +
                ", armor=" + armor +
                ", weapons=" + weapons +
                '}';
    }
}
