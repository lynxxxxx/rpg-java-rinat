package character;

import enums.ArmorTypes;
import enums.WeaponsTypes;
import attributes.PrimaryAttributes;
import attributes.TotalAttributes;
import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import items.Armor;
import items.Weapons;
import enums.Slot;


public class Mage extends Character {

    public Mage(String name, double level, double characterDPS, PrimaryAttributes primaryAttributes,
                TotalAttributes totalAttributes, Armor armor, Weapons weapons) {
        super(name, level, characterDPS, primaryAttributes, totalAttributes, armor, weapons);
    }


    /**
     * each time the function is called levels up by 1
     * each time the function is called, the levels increase by 1 and the primaryAttribute increases in value.
     * checks if character has weapons attributes if yes increases character's weaponsDPS outgoing from weapon value
     * and charactersDPS : characterDPS = weapons.getWeaponDPS() * (1 + getPrimaryAttributes().getIntelligence() / 100)
     */
    @Override
    public void levelUp() {
        level++;
        primaryAttributes.setStrength(primaryAttributes.getStrength() + 1);
        primaryAttributes.setDexterity(primaryAttributes.getDexterity() + 1);
        primaryAttributes.setIntelligence(primaryAttributes.getIntelligence() + 5);
        if (getWeapons().getSlotWeapons().size() > 0) {
            if (getWeapons().getSlotWeapons().containsValue(WeaponsTypes.Staffs)) {
                WeaponsTypes.Staffs.setDamage(WeaponsTypes.Staffs.getDamage() + 5f / 100);
                weapons.setWeaponDPS(weapons.getWeaponDPS() + 5f / 100 * WeaponsTypes.Staffs.getAttacksPerSecond());
            } else {
                WeaponsTypes.Wands.setDamage(WeaponsTypes.Wands.getDamage() + 5f / 100);
                weapons.setWeaponDPS(weapons.getWeaponDPS() + 5f / 100 * WeaponsTypes.Wands.getAttacksPerSecond());
            }


            weapons.setWeaponDPS(Math.round(weapons.getWeaponDPS() * 100) / 100.0);
            countOutCharacterDPS();

        }

        if (this.getTotalAttributes().getDexterity() == 0 &&
                this.getTotalAttributes().getStrength() == 0 &&
                this.getTotalAttributes().getIntelligence() == 0
        ) {
            totalAttributes.setStrength(primaryAttributes.getStrength());
            totalAttributes.setDexterity(primaryAttributes.getDexterity());
            totalAttributes.setIntelligence(primaryAttributes.getIntelligence());
        } else {
            this.getTotalAttributes().setStrength(totalAttributes.getStrength() + 1);
            this.getTotalAttributes().setDexterity(totalAttributes.getDexterity() + 1);
            this.getTotalAttributes().setIntelligence(totalAttributes.getIntelligence() + 5);
        }


    }

    /**
     * checks if the added armor attribute is valid, if yes  the armor attribute value
     * is retrieved and added to the character's main armor attributes
     *
     * @throws InvalidArmorException if added armor attribute is not valid exception is thrown
     */
    @Override
    public void addArmorAttributes() throws InvalidArmorException {
        if (armor.getSlotArmor().size() == 0) {
            armor.getSlotArmor().put(Slot.Legs, ArmorTypes.Cloth);
            armor.getSlotArmor().put(Slot.Body, ArmorTypes.Cloth);
            armor.getSlotArmor().put(Slot.Head, ArmorTypes.Cloth);
        }

        if (getName().equals("Mage") && getArmor().getRequiredLevel() >= 2 &&
                !getArmor().getSlotArmor().containsValue(ArmorTypes.Mail) &&
                !getArmor().getSlotArmor().containsValue(ArmorTypes.Leather) &&
                !getArmor().getSlotArmor().containsValue(ArmorTypes.Plate) &&
                !getArmor().getSlotArmor().containsKey(Slot.Weapon)
        ) {
            for (int i = 0; i < getArmor().getSlotArmor().size(); i++) {
                this.getArmor().getPrimaryAttributes().setStrength(getArmor().getSlotArmor()
                        .values().stream().mapToDouble(ArmorTypes::getStrength).sum());
                this.getArmor().getPrimaryAttributes().setDexterity(getArmor().getSlotArmor()
                        .values().stream().mapToDouble(ArmorTypes::getDexterity).sum());
                this.getArmor().getPrimaryAttributes().setIntelligence(getArmor().getSlotArmor()
                        .values().stream().mapToDouble(ArmorTypes::getIntelligence).sum());

            }
        } else {
            throw new InvalidArmorException("To low level or Invalid ArmorType, Mage can only have: " + ArmorTypes.Cloth.getItem());
        }
    }


    /**
     * calculates the total value of the armor's attributes:
     * Characters PrimaryAttributes + Character getArmor().getPrimaryAttribute
     */
    @Override
    public void getAttributesTotalSum() {
        if (!getArmor().getSlotArmor().containsValue(ArmorTypes.Mail) &&
                !getArmor().getSlotArmor().containsValue(ArmorTypes.Leather) &&
                !getArmor().getSlotArmor().containsValue(ArmorTypes.Plate) &&
                !getArmor().getSlotArmor().containsKey(Slot.Weapon)
        ) {
            for (int i = 0; i < getArmor().getSlotArmor().size(); i++) {
                this.getTotalAttributes().setStrength(getPrimaryAttributes().getStrength() +
                        getArmor().getPrimaryAttributes().getStrength());
                this.getTotalAttributes().setDexterity(getPrimaryAttributes().getDexterity() +
                        getArmor().getPrimaryAttributes().getDexterity());
                this.getTotalAttributes().setIntelligence(getPrimaryAttributes().getIntelligence() +
                        getArmor().getPrimaryAttributes().getIntelligence());
            }

        } else {
            throw new NullPointerException("you entered failed type, Mage can only have: " + ArmorTypes.Cloth.getItem());
        }
    }


    /**
     * checks if the attribute of the added weapon is valid, if yes,
     * based on the value of the added weapon, the DPS of the weapon is calculated
     *
     * @throws InvalidWeaponException if added weapon attribute is not valid exception is thrown
     */
    @Override
    public void addWeaponAttributes() throws InvalidWeaponException {
        if (weapons.getSlotWeapons().size() == 0) {
            weapons.getSlotWeapons().put(Slot.Weapon, WeaponsTypes.Staffs);
        }

        if (getName().equals("Mage") && !getWeapons().getSlotWeapons().containsValue(WeaponsTypes.Axes) &&
                getWeapons().getRequiredLevel() >= 2 &&
                !getWeapons().getSlotWeapons().containsValue(WeaponsTypes.Bows) &&
                !getWeapons().getSlotWeapons().containsValue(WeaponsTypes.Daggers) &&
                !getWeapons().getSlotWeapons().containsValue(WeaponsTypes.Hammers) &&
                !getWeapons().getSlotWeapons().containsValue(WeaponsTypes.Swords) &&
                !getWeapons().getSlotWeapons().containsKey(Slot.Head) &&
                !getWeapons().getSlotWeapons().containsKey(Slot.Body) &&
                !getWeapons().getSlotWeapons().containsKey(Slot.Legs)
        ) {
            double weaponsCurrentDPSValue = weapons.getWeaponDPS();
            if (getWeapons().getSlotWeapons().containsValue(WeaponsTypes.Staffs)) {
                weapons.setWeaponDPS(WeaponsTypes.Staffs.getDamage() * WeaponsTypes.Staffs.getAttacksPerSecond() + weaponsCurrentDPSValue);
                weapons.setWeaponDPS(Math.round(weapons.getWeaponDPS() * 100) / 100.0);
            } else {
                weapons.setWeaponDPS(WeaponsTypes.Wands.getDamage() * WeaponsTypes.Wands.getAttacksPerSecond() + weaponsCurrentDPSValue);
                weapons.setWeaponDPS(Math.round(weapons.getWeaponDPS() * 100) / 100.0);
            }


        } else {
            throw new InvalidWeaponException("To low level or Invalid WeaponsType, Mage can only have: " + WeaponsTypes.Staffs.getItem()
                    + " or " + WeaponsTypes.Wands.getItem() + " and can only have Slot: " + Slot.Weapon);
        }

    }


    /**
     * checks if the character has a weapon attribute,
     * if not adds the default weapon DPS of 1
     */
    @Override
    public void checkIfHeroHaveWeaponAttributes() {
        if (weapons.getSlotWeapons().size() == 0) {
            weapons.setWeaponDPS(1);
        }

    }


    /**
     * calculates the Character's DPS
     */
    @Override
    public void countOutCharacterDPS() {
        if (getArmor().getSlotArmor().size() == 0) {
            characterDPS = weapons.getWeaponDPS() * (1 + getPrimaryAttributes().getIntelligence() / 100);
        } else {
            characterDPS = weapons.getWeaponDPS() * (1 +
                    (getPrimaryAttributes().getIntelligence() + getArmor().getPrimaryAttributes().getIntelligence()) / 100);
        }
        characterDPS = Math.round(characterDPS * 100) / 100.0;

    }

    @Override
    public String toString() {
        return "Mage{" +
                "name='" + name + '\'' +
                ", level=" + level +
                ", characterDPS=" + characterDPS +
                ", primaryAttributes=" + primaryAttributes +
                ", totalAttributes=" + totalAttributes +
                ", armor=" + armor +
                ", weapons=" + weapons +
                '}';
    }
}


