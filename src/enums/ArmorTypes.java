package enums;

/**
 *Each armor has its own value
 */
public enum ArmorTypes {
        Cloth("Cloth",1,3,2),
        Leather("Leather", 2 ,4,3),
        Mail("Mail",2,1,4),
        Plate("Plate",1,2,3);

        String Item;
        private double strength;
        private double dexterity;
        private double intelligence;

        ArmorTypes(String item, double strength, double dexterity, double intelligence) {
                Item = item;
                this.strength = strength;
                this.dexterity = dexterity;
                this.intelligence = intelligence;
        }

        public String getItem() {
                return Item;
        }

        public void setItem(String item) {
                Item = item;
        }

        public double getStrength() {
                return strength;
        }

        public void setStrength(double strength) {
                this.strength = strength;
        }

        public double getDexterity() {
                return dexterity;
        }

        public void setDexterity(double dexterity) {
                this.dexterity = dexterity;
        }

        public double getIntelligence() {
                return intelligence;
        }

        public void setIntelligence(double intelligence) {
                this.intelligence = intelligence;
        }

}
