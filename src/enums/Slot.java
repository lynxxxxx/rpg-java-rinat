package enums;

public enum Slot {
    Head,
    Body,
    Legs,
    Weapon
}
