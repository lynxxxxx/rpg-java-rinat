package enums;


/**
 *Each Weapon has its own value
 */
public enum WeaponsTypes {
     Axes("Axes",7,1.1),
     Bows("Bows",14,1.5),
     Daggers("Daggers",16,2),
     Hammers("Hammers",15,2.5),
     Staffs("Staffs",14,3.3),
     Swords("Swords",20,2.2),
     Wands("Wands",18,1.9);


     String Item;
     private double damage;
     private double attacksPerSecond;


    WeaponsTypes(String item, double damage, double attacksPerSecond) {
        Item = item;
        this.damage = damage;
        this.attacksPerSecond = attacksPerSecond;
    }

    public String getItem() {
        return Item;
    }

    public void setItem(String item) {
        Item = item;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = Math.round(damage *100)/100.0;
    }

    public double getAttacksPerSecond() {
        return attacksPerSecond;
    }

    public void setAttacksPerSecond(double attacksPerSecond) {
        this.attacksPerSecond = attacksPerSecond;
    }

    @Override
    public String toString() {
        return "WeaponsTypes{" +
                "Item='" + Item + '\'' +
                ", damage=" + damage +
                ", attacksPerSecond=" + attacksPerSecond +
                '}';
    }
}
