package exception;



public class InvalidArmorException extends Exception{
  private String message;

    public InvalidArmorException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "InvalidArmorException{" +
                "message='" + message + '\'' +
                '}';
    }
}
