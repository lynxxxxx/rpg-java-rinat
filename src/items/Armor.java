package items;

import enums.ArmorTypes;
import attributes.PrimaryAttributes;
import enums.Slot;

import java.util.HashMap;

public class Armor extends Item{

     private PrimaryAttributes primaryAttributes;


    public Armor(String name, double requiredLevel, HashMap<Slot, ArmorTypes> slotArmor, PrimaryAttributes primaryAttributes) {
        super(name, requiredLevel, slotArmor);
        this.primaryAttributes = primaryAttributes;
    }

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public void setPrimaryAttributes(PrimaryAttributes primaryAttributes) {
        this.primaryAttributes = primaryAttributes;
    }

    @Override
    public String toString() {
        return "Armor{" +
                "primaryAttributes=" + primaryAttributes +
                ", name='" + name + '\'' +
                ", requiredLevel=" + requiredLevel +
                ", slotArmor=" + slotArmor +
                '}';
    }
}
