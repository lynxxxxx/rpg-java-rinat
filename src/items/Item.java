package items;

import enums.ArmorTypes;
import enums.WeaponsTypes;
import enums.Slot;

import java.util.HashMap;


public abstract class Item {

    protected String name;
    protected double requiredLevel;

   protected HashMap<Slot, ArmorTypes> slotArmor;
   protected HashMap<Slot, WeaponsTypes> slotWeapons;

    /**
     *this constructor is used by Armor
     * @param name
     * @param requiredLevel
     * @param slotArmor
     */
    public Item(String name, double requiredLevel, HashMap<Slot, ArmorTypes> slotArmor) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slotArmor = slotArmor;
    }

    /**
     * this constructor is used by Weapon
     * @param requiredLevel
     * @param slotWeapons
     */
    public Item(double requiredLevel, HashMap<Slot, WeaponsTypes> slotWeapons) {
        this.requiredLevel = requiredLevel;
        this.slotWeapons = slotWeapons;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(double requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public HashMap<Slot, ArmorTypes> getSlotArmor() {
        return slotArmor;
    }

    public void setSlotArmor(HashMap<Slot, ArmorTypes> slotArmor) {
        this.slotArmor = slotArmor;
    }

    public HashMap<Slot, WeaponsTypes> getSlotWeapons() {
        return slotWeapons;
    }

    public void setSlotWeapons(HashMap<Slot, WeaponsTypes> slotWeapons) {
        this.slotWeapons = slotWeapons;
    }

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", requiredLevel=" + requiredLevel +
                ", slotArmor=" + slotArmor +
                ", slotWeapons=" + slotWeapons +
                '}';
    }
}
