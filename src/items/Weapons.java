package items;

import enums.WeaponsTypes;
import enums.Slot;

import java.util.HashMap;

public class Weapons extends Item{

   private double weaponDPS;

    public Weapons( double requiredLevel, HashMap<Slot, WeaponsTypes> slotWeapons, double weaponDPS) {
        super( requiredLevel, slotWeapons);
        this.weaponDPS = weaponDPS;
    }

    public double getWeaponDPS() {
        return weaponDPS;
    }

    public void setWeaponDPS(double weaponDPS) {
        this.weaponDPS = weaponDPS;
    }

    @Override
    public String toString() {
        return "Weapons{" +
                ", weaponDPS=" + weaponDPS +
                ", requiredLevel=" + requiredLevel +
                ", slotWeapons=" + slotWeapons +
                '}';
    }
}
