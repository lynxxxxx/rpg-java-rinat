package character;

import attributes.PrimaryAttributes;
import attributes.TotalAttributes;
import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import items.Armor;
import items.Weapons;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {


    private Mage mage;
    private Ranger ranger;
    private Rogue rogue;
    private Warrior warrior;

    @BeforeEach
  void setUp() {
      mage = new Mage("Mage",1,0,new PrimaryAttributes(1,1,8),
                new TotalAttributes(0,0,0),
                new Armor("Armor",2,new HashMap<>(),new PrimaryAttributes(0,0,0)),
                new Weapons(2,new HashMap<>(),0)
        );

      ranger = new Ranger("Ranger",1,0,new PrimaryAttributes(1,7,1),
              new TotalAttributes(0,0,0),
              new Armor("Armor",2,new HashMap<>(),new PrimaryAttributes(0,0,0)),
              new Weapons(2,new HashMap<>(),0)
      );

      rogue = new Rogue("Rogue",1,0,new PrimaryAttributes(2,6,1),
                new TotalAttributes(0,0,0),
                new Armor("Armor",2,new HashMap<>(),new PrimaryAttributes(0,0,0)),
                new Weapons(2,new HashMap<>(),0)
        );

      warrior = new Warrior("Warrior",1,0,new PrimaryAttributes(5,2,1),
              new TotalAttributes(0,0,0),
              new Armor("Armor",2,new HashMap<>(),new PrimaryAttributes(0,0,0)),
              new Weapons(2,new HashMap<>(),0)
      );

    }


    //1
    @Test
    void Level_IfMageLevelIs1AfterCreated_Expected1() {
        // Assert
       assertEquals(mage.getLevel(),1 );
    }

    @Test
    void Level_IfRangeLevelIs1AfterCreated_Expected1() {
        // Assert
        assertEquals(ranger.getLevel(),1 );
    }

    @Test
    void Level_IfRogueLevelIs1AfterCreated_Expected1() {
        // Assert
        assertEquals(rogue.getLevel(),1 );
    }

    @Test
    void Level_IfWarriorLevelIs1AfterCreated_Expected1() {
        // Assert
        assertEquals(warrior.getLevel(),1 );
    }




    //2
    @Test
    void LevelUp_IfMageLevelIs2AfterLevelIncrease_Expected2() {
        //Act
         mage.levelUp();

        // Assert
        assertEquals(mage.getLevel(), 2);
    }

    @Test
    void LevelUp_IfRangerLevelIs2AfterLevelIncrease_Expected2() {
        //Act
        ranger.levelUp();

        // Assert
        assertEquals(ranger.getLevel(), 2);
    }

    @Test
    void LevelUp_IfRogueLevelIs2AfterLevelIncrease_Expected2() {
        //Act
        rogue.levelUp();

        // Assert
        assertEquals(rogue.getLevel(), 2);
    }

    @Test
    void LevelUp_IfWarriorLevelIs2AfterLevelIncrease_Expected2() {
        //Act
        warrior.levelUp();

        // Assert
        assertEquals(warrior.getLevel(), 2);
    }


    //3
    @Test
    void DefaultAttributes_IfMageHaveDefaultAttributesAfterCreating() {
        //Act
     PrimaryAttributes primaryAttributes = new PrimaryAttributes(1,1,8);
        // Assert
        assertEquals(mage.getPrimaryAttributes(), primaryAttributes);

    }

    @Test
    void DefaultAttributes_IfRangerHaveDefaultAttributesAfterCreating() {
        //Act
         PrimaryAttributes primaryAttributes = new PrimaryAttributes(1,7,1);

        // Assert
        assertEquals(ranger.getPrimaryAttributes(), primaryAttributes);

    }

    @Test
    void DefaultAttributes_IfRogueHaveDefaultAttributesAfterCreating() {
        //Act
       PrimaryAttributes primaryAttributes = new PrimaryAttributes(2,6,1);
        // Assert
        assertEquals(rogue.getPrimaryAttributes(), primaryAttributes);
    }

    @Test
    void DefaultAttributes_IfWarriorHaveDefaultAttributesAfterCreating() {
        //Act
       PrimaryAttributes primaryAttributes = new PrimaryAttributes(5,2,1);

        // Assert
        assertEquals(warrior.getPrimaryAttributes(), primaryAttributes);

    }




    //4
    @Test
    void LevelUp_IfMageBaseAttributesIncreaseAfterMageLevelUp_AttributeIncrease() {
        //Act
       mage.levelUp();
        PrimaryAttributes primaryAttributes = new PrimaryAttributes(2,2,13);
        // Assert
        assertEquals(mage.getPrimaryAttributes(), primaryAttributes);

    }

    @Test
    void LevelUp_IfRangerBaseAttributesIncreaseAfterMageLevelUp_AttributeIncrease() {
        //Act
        ranger.levelUp();
        PrimaryAttributes primaryAttributes = new PrimaryAttributes(2,12,2);

        // Assert
        assertEquals(ranger.getPrimaryAttributes(),primaryAttributes);
    }

    @Test
    void LevelUp_IfRogueBaseAttributesIncreaseAfterMageLevelUp_AttributeIncrease() {
        //Act
        rogue.levelUp();
        PrimaryAttributes primaryAttributes = new PrimaryAttributes(3,10,2);

        // Assert
        assertEquals(rogue.getPrimaryAttributes(), primaryAttributes);
    }

    @Test
    void LevelUp_IfWarriorBaseAttributesIncreaseAfterMageLevelUp_AttributeIncrease() {
        //Act
        warrior.levelUp();
        PrimaryAttributes primaryAttributes = new PrimaryAttributes(8,4,2);
        // Assert
        assertEquals(warrior.getPrimaryAttributes(), primaryAttributes);
    }

}