package items;

import enums.ArmorTypes;
import enums.WeaponsTypes;
import attributes.PrimaryAttributes;
import attributes.TotalAttributes;
import character.Warrior;
import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import enums.Slot;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    private Warrior warrior;

    @BeforeEach
    void setUp() {
        warrior = new Warrior("Warrior",1,0,new PrimaryAttributes(5,2,1),
                new TotalAttributes(0,0,0),
                new Armor("Warrior",2,new HashMap<>(),new PrimaryAttributes(0,0,0)),
                new Weapons(2,new HashMap<>(),0)
        );
    }


    @Test
    void InvalidWeaponException_IfCharacterTriesToEquipHigherLevelWeapon_ThrowExceptionTrue(){
        // Arrange
        warrior.getWeapons().setRequiredLevel(1);
        String expected = "To low level or Invalid WeaponsType, Warrior can only have: " + WeaponsTypes.Axes.getItem()
                + "," + WeaponsTypes.Swords.getItem()  + " or " + WeaponsTypes.Hammers.getItem() + " and can only have Slot: " + Slot.Weapon;

        //Act
        InvalidWeaponException exception = assertThrows(InvalidWeaponException.class, () -> warrior.addWeaponAttributes());
        String actual = exception.getMessage();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void InvalidArmorException_IfCharacterTriesToEquipHigherLevelArmor_ThrowExceptionTrue() throws InvalidArmorException {
        // Arrange
        warrior.getArmor().setRequiredLevel(1);
        String expected = "To low level or Invalid ArmorType, Warrior can only have: " +
                ArmorTypes.Plate + " or " + ArmorTypes.Mail;

        //Act
        InvalidArmorException exception = assertThrows(InvalidArmorException.class, () -> warrior.addArmorAttributes());
        String actual = exception.getMessage();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void InvalidWeaponException_IfCharacterTriesToEquipWrongWeaponType_ThrowExceptionTrue(){
        // Arrange
        warrior.getWeapons().getSlotWeapons().put(Slot.Weapon, WeaponsTypes.Bows);
        String expected = "To low level or Invalid WeaponsType, Warrior can only have: " + WeaponsTypes.Axes.getItem()
                + "," + WeaponsTypes.Swords.getItem()  + " or " + WeaponsTypes.Hammers.getItem() + " and can only have Slot: " + Slot.Weapon;

        //Act
        InvalidWeaponException exception = assertThrows(InvalidWeaponException.class, () -> warrior.addWeaponAttributes());
        String actual = exception.getMessage();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void InvalidArmorException_IfCharacterTriesToEquipWrongArmorType_ThrowExceptionTrue(){
        // Arrange
        warrior.getArmor().getSlotArmor().put(Slot.Body, ArmorTypes.Cloth);
        String expected = "To low level or Invalid ArmorType, Warrior can only have: " +
                ArmorTypes.Plate + " or " + ArmorTypes.Mail;

        //Act
        InvalidArmorException exception = assertThrows(InvalidArmorException.class, () -> warrior.addArmorAttributes());
        String actual = exception.getMessage();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void AddWeaponAttributes_IfCharacterTriesToEquipAValidWeapon_True() throws InvalidWeaponException {
        // Arrange
       warrior.addWeaponAttributes();

        //Act
       boolean validWeapon = warrior.getWeapons().getSlotWeapons().containsValue(WeaponsTypes.Axes) ||
                    warrior.getWeapons().getSlotWeapons().containsValue(WeaponsTypes.Hammers) ||
                    warrior.getWeapons().getSlotWeapons().containsValue(WeaponsTypes.Swords);

        //Assert
        assertTrue(validWeapon);
    }

    @Test
    void AddArmorAttributes_IfCharacterTriesToEquipAValidArmorPiece_True() throws InvalidArmorException {
        // Arrange
        warrior.addArmorAttributes();

        //Act
        boolean validArmorPiece = warrior.getArmor().getSlotArmor().containsValue(ArmorTypes.Plate) ||
                                  warrior.getArmor().getSlotArmor().containsValue(ArmorTypes.Mail);

        //Assert
        assertTrue(validArmorPiece);
    }

    @Test
    void CheckIfHeroHaveWeaponAttributes_CalculateDPSIgNoWeaponIsEquipped_ExpectedTrue() {
        // Arrange
        warrior.checkIfHeroHaveWeaponAttributes();
        warrior.countOutCharacterDPS();

        //Assert
        assertEquals(warrior.getCharacterDPS(), 1.05);
    }

    @Test
    void CountOutCharacterDPS_CalculateDPSWithValidWeaponEquipped_ExpectedTrue() throws InvalidWeaponException {
        // Arrange
        warrior.addWeaponAttributes();
        warrior.countOutCharacterDPS();

        //Assert
        assertEquals(warrior.getCharacterDPS(), 8.09);
    }

    @Test
    void CountOutCharacterDPS_CalculateDPSWithValidWeaponAndArmorEquipped_True() throws InvalidWeaponException, InvalidArmorException {
        // Arrange
        warrior.addWeaponAttributes();
        warrior.addArmorAttributes();
        warrior.countOutCharacterDPS();

        //Assert
        assertEquals(warrior.getCharacterDPS(), 8.16);
    }
}

